console.log('👩🏻‍💻 S18 - Activity Console 👩🏻‍💻'); // test connectivity 

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

// [START OF ANSWERS]
// 1.
// Add
function sumOfTwoNums(num1, num2) {
    console.log(`Displayed sum of ${num1} and ${num2}`);
    console.log(num1 + num2);
};

sumOfTwoNums(5, 15);


// Subtract
function differenceOfTwoNums(num1, num2) {
    console.log(`Displayed difference of ${num1} and ${num2}`);
    console.log(num1-num2);
}

differenceOfTwoNums(20,5);


// 2.
let testNum1 = 50;
let testNum2 = 10;
// Multiply
const productOfTwoNums = (num1, num2) => num1 * num2;

// Divide
const quotientOfTwoNums = (num1, num2) => num1 / num2;

let product = productOfTwoNums(testNum1,testNum2);
let quotient = quotientOfTwoNums(testNum1,testNum2);

// Product
console.log(`The product of ${testNum1} and ${testNum2}:`);
console.log(product);

// Quotient
console.log(`The quotient of ${testNum1} and ${testNum2}:`)
console.log(quotient);


// 3. A = PI(r)**2
let inputRadius = 15;
const areaOfCircle = (radius) => {
    return parseFloat((Math.PI * (radius**2)).toFixed(2));
};

let circleArea = areaOfCircle(inputRadius);

console.log(`The result of getting the area of a circle with ${inputRadius} radius:`);

console.log(circleArea);


// 4. 
let inputNum1 = 20;
let inputNum2 = 40;
let inputNum3 = 60;
let inputNum4 = 80;

function getAverageOf(num1, num2, num3, num4) {
    return (num1 + num2 + num3 + num4) / 4;
}

let averageVar = getAverageOf(inputNum1, inputNum2, inputNum3, inputNum4);

console.log(`The average of ${inputNum1}, ${inputNum2}, ${inputNum3} and ${inputNum4}:`);

console.log(averageVar);


// 5.
let yourScore = parseInt(prompt('Enter your score'));
let totalScore = parseInt(prompt('Enter the total test score'));

function computeScoreIfPass(yourScore, totalScore) {
    let scorePercentage = (yourScore / totalScore) * 100;
    let isPassed = scorePercentage >= 75;

    return isPassed;
};

let isPassingScore = computeScoreIfPass(yourScore, totalScore);

console.log(`Is ${yourScore}/${totalScore} a passing score?`);
console.log(isPassingScore);